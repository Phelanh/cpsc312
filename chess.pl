move(piece(white, FromType, FromPos), To, board(white, Board), board(black, NewBoard)) :-
  move_is_valid(piece(white, FromType, FromPos), To, Board),
  make_move(piece(white, FromType, FromPos), To, Board, NewBoard),
  \+ in_check(white, NewBoard).

move(piece(black, FromType, FromPos), To, board(black, Board), board(white, NewBoard)) :-
  move_is_valid(piece(black, FromType, FromPos), To, Board),
  make_move(piece(black, FromType, FromPos), To, Board, NewBoard),
  \+ in_check(black, NewBoard).



in_check(Color, Board) :- 
  member(piece(Color, king, KingPos), Board),
  member(piece(OtherColor, Type, FromPos), Board),
  dif(Color, OtherColor),
  move_is_valid(piece(OtherColor, Type, FromPos), piece(OtherColor, Type, KingPos), Board).


move_is_valid(piece(Color, king, FromPos), piece(Color, king, ToPos), Board) :-
  column(_, _, ColumnList),
  adjacent(ToPos, FromPos, ColumnList),
  laneway_empty(Color, FromPos, ToPos, ColumnList, Board).  
move_is_valid(piece(Color, king, FromPos), piece(Color, king, ToPos), Board) :-
  column(_, _, ColumnList),
  adjacent(FromPos, ToPos, ColumnList),
  laneway_empty(Color, FromPos, ToPos, ColumnList, Board).  
move_is_valid(piece(Color, king, FromPos), piece(Color, king, ToPos), Board) :-
  row(_, RowList),
  adjacent(ToPos, FromPos, RowList),
  laneway_empty(Color, FromPos, ToPos, RowList, Board).  
move_is_valid(piece(Color, king, FromPos), piece(Color, king, ToPos), Board) :-
  row(_, RowList),
  adjacent(FromPos, ToPos, RowList),
  laneway_empty(Color, FromPos, ToPos, RowList, Board).  
move_is_valid(piece(Color, king, FromPos), piece(Color, king, ToPos), Board) :-
  diagonal(DiagonalList),
  adjacent(ToPos, FromPos, DiagonalList),
  laneway_empty(Color, FromPos, ToPos, DiagonalList, Board).  
move_is_valid(piece(Color, king, FromPos), piece(Color, king, ToPos), Board) :-
  diagonal(DiagonalList),
  adjacent(FromPos, ToPos, DiagonalList),
  laneway_empty(Color, FromPos, ToPos, DiagonalList, Board).
  

move_is_valid(piece(Color, queen, FromPos), piece(Color, queen, ToPos), Board) :-
  row(_, RowList),
  member(FromPos, RowList),
  member(ToPos, RowList),
  laneway_empty(Color, FromPos, ToPos, RowList, Board).
move_is_valid(piece(Color, queen, FromPos), piece(Color, queen, ToPos), Board) :-
  column(_, _, ColumnList),
  member(FromPos, ColumnList),
  member(ToPos, ColumnList),
  laneway_empty(Color, FromPos, ToPos, ColumnList, Board).
move_is_valid(piece(Color, queen, FromPos), piece(Color, queen, ToPos), Board) :-
  diagonal(DiagonalList),
  member(FromPos, DiagonalList),
  member(ToPos, DiagonalList),
  laneway_empty(Color, FromPos, ToPos, DiagonalList, Board).

move_is_valid(piece(Color, rook, FromPos), piece(Color, rook, ToPos), Board) :-
  row(_, RowList),
  member(FromPos, RowList),
  member(ToPos, RowList),
  laneway_empty(Color, FromPos, ToPos, RowList, Board).  
move_is_valid(piece(Color, rook, FromPos), piece(Color, rook, ToPos), Board) :-
  column(_, _, ColumnList),
  member(FromPos, ColumnList),
  member(ToPos, ColumnList),
  laneway_empty(Color, FromPos, ToPos, ColumnList, Board).  

move_is_valid(piece(Color, bishop, FromPos), piece(Color, bishop, ToPos), Board) :-
  diagonal(DiagonalList),
  member(FromPos, DiagonalList),
  member(ToPos, DiagonalList),
  laneway_empty(Color, FromPos, ToPos, DiagonalList, Board).  

move_is_valid(piece(Color, knight, FromPos), piece(Color, knight, ToPos), Board) :-
  l_move(FromPos, ToPos),
  \+ member(piece(Color, _, ToPos), Board).

move_is_valid(piece(white, pawn, FromPos), piece(white, pawn, ToPos), Board) :-
  column(_, _, ColumnList),
  adjacent(FromPos, ToPos, ColumnList),
  laneway_empty_without_take(FromPos, ToPos, ColumnList, Board).
move_is_valid(piece(black, pawn, FromPos), piece(black, pawn, ToPos), Board) :-
  column(_, _, ColumnList),
  reverse(ColumnList, ReverseColumnList),
  adjacent(FromPos, ToPos, ReverseColumnList),
  laneway_empty_without_take(FromPos, ToPos, ReverseColumnList, Board).

move_is_valid(piece(white, pawn, FromPos), piece(white, pawn, ToPos), Board) :-
  diagonal(DiagonalList),
  adjacent(FromPos, ToPos, DiagonalList),
  laneway_empty_with_take(white, FromPos, ToPos, DiagonalList, Board).
move_is_valid(piece(black, pawn, FromPos), piece(black, pawn, ToPos), Board) :-
  diagonal(DiagonalList),
  reverse(DiagonalList, ReverseDiagonalList),
  adjacent(FromPos, ToPos, ReverseDiagonalList),
  laneway_empty_with_take(black, FromPos, ToPos, ReverseDiagonalList, Board).

move_is_valid(piece(white, pawn, pos(Column, 2)), piece(white, pawn, pos(Column, 4)), Board) :-
  column(_, Column, ColumnList),
  laneway_empty_without_take(pos(Column, 2), pos(Column, 4), ColumnList, Board).
move_is_valid(piece(black, pawn, pos(Column, 7)), piece(black, pawn, pos(Column, 5)), Board) :-
  column(_, Column, ColumnList),
  laneway_empty_without_take(pos(Column, 7), pos(Column, 5), ColumnList, Board).


make_move(_, _, [], []).
make_move(piece(Color, Type, OldPos), piece(Color, Type, NewPos), [piece(Color1, Type1, Pos1) | B1], [piece(Color1, Type1, Pos1) | B2]) :-
  dif(NewPos, Pos1),
  dif(OldPos, Pos1),
  make_move(piece(Color, Type, OldPos), piece(Color, Type, NewPos), B1, B2).
make_move(piece(Color, Type, OldPos), piece(Color, Type, NewPos), [piece(Color, Type, OldPos) | B1], [piece(Color, Type, NewPos) | B2]) :-
  make_move(piece(Color, Type, OldPos), piece(Color, Type, NewPos), B1, B2).
make_move(piece(Color, Type, OldPos), piece(Color, Type, NewPos), [piece(_, _, NewPos) | B1], B2) :-
  make_move(piece(Color, Type, OldPos), piece(Color, Type, NewPos), B1, B2).


% Board/Move Definitions:
column(1, a, [pos(a, 1), pos(a, 2), pos(a, 3), pos(a, 4), pos(a, 5), pos(a, 6), pos(a, 7), pos(a, 8)]).
column(2, b, [pos(b, 1), pos(b, 2), pos(b, 3), pos(b, 4), pos(b, 5), pos(b, 6), pos(b, 7), pos(b, 8)]).
column(3, c, [pos(c, 1), pos(c, 2), pos(c, 3), pos(c, 4), pos(c, 5), pos(c, 6), pos(c, 7), pos(c, 8)]).
column(4, d, [pos(d, 1), pos(d, 2), pos(d, 3), pos(d, 4), pos(d, 5), pos(d, 6), pos(d, 7), pos(d, 8)]).
column(5, e, [pos(e, 1), pos(e, 2), pos(e, 3), pos(e, 4), pos(e, 5), pos(e, 6), pos(e, 7), pos(e, 8)]).
column(6, f, [pos(f, 1), pos(f, 2), pos(f, 3), pos(f, 4), pos(f, 5), pos(f, 6), pos(f, 7), pos(f, 8)]).
column(7, g, [pos(g, 1), pos(g, 2), pos(g, 3), pos(g, 4), pos(g, 5), pos(g, 6), pos(g, 7), pos(g, 8)]).
column(8, h, [pos(h, 1), pos(h, 2), pos(h, 3), pos(h, 4), pos(h, 5), pos(h, 6), pos(h, 7), pos(h, 8)]).

row(1, [pos(a, 1), pos(b, 1), pos(c, 1), pos(d, 1), pos(e, 1), pos(f, 1), pos(g, 1), pos(h, 1)]).
row(2, [pos(a, 2), pos(b, 2), pos(c, 2), pos(d, 2), pos(e, 2), pos(f, 2), pos(g, 2), pos(h, 2)]).
row(3, [pos(a, 3), pos(b, 3), pos(c, 3), pos(d, 3), pos(e, 3), pos(f, 3), pos(g, 3), pos(h, 3)]).
row(4, [pos(a, 4), pos(b, 4), pos(c, 4), pos(d, 4), pos(e, 4), pos(f, 4), pos(g, 4), pos(h, 4)]).
row(5, [pos(a, 5), pos(b, 5), pos(c, 5), pos(d, 5), pos(e, 5), pos(f, 5), pos(g, 5), pos(h, 5)]).
row(6, [pos(a, 6), pos(b, 6), pos(c, 6), pos(d, 6), pos(e, 6), pos(f, 6), pos(g, 6), pos(h, 6)]).
row(7, [pos(a, 7), pos(b, 7), pos(c, 7), pos(d, 7), pos(e, 7), pos(f, 7), pos(g, 7), pos(h, 7)]).
row(8, [pos(a, 8), pos(b, 8), pos(c, 8), pos(d, 8), pos(e, 8), pos(f, 8), pos(g, 8), pos(h, 8)]).



% all diagonal lists move 'up' the board, in the direction white's pawns can take
diagonal([pos(a, 8)]).
diagonal([pos(a, 7), pos(b, 8)]).
diagonal([pos(a, 6), pos(b, 7), pos(c, 8)]).
diagonal([pos(a, 5), pos(b, 6), pos(c, 7), pos(d, 8)]).
diagonal([pos(a, 4), pos(b, 5), pos(c, 6), pos(d, 7), pos(e, 8)]).
diagonal([pos(a, 3), pos(b, 4), pos(c, 5), pos(d, 6), pos(e, 7), pos(f, 8)]).
diagonal([pos(a, 2), pos(b, 3), pos(c, 4), pos(d, 5), pos(e, 6), pos(f, 7), pos(g, 8)]).
diagonal([pos(a, 1), pos(b, 2), pos(c, 3), pos(d, 4), pos(e, 5), pos(f, 6), pos(g, 7), pos(h, 8)]).
diagonal([pos(b, 1), pos(c, 2), pos(d, 3), pos(e, 4), pos(f, 5), pos(g, 6), pos(h, 7)]).
diagonal([pos(c, 1), pos(d, 2), pos(e, 3), pos(f, 4), pos(g, 5), pos(h, 6)]).
diagonal([pos(d, 1), pos(e, 2), pos(f, 3), pos(g, 4), pos(h, 5)]).
diagonal([pos(e, 1), pos(f, 2), pos(g, 3), pos(h, 4)]).
diagonal([pos(f, 1), pos(g, 2), pos(h, 3)]).
diagonal([pos(g, 1), pos(h, 2)]).
diagonal([pos(h, 1)]).

diagonal([pos(a, 1)]).
diagonal([pos(b, 1), pos(a, 2)]).
diagonal([pos(c, 1), pos(b, 2), pos(a, 3)]).
diagonal([pos(d, 1), pos(c, 2), pos(b, 3), pos(a, 4)]).
diagonal([pos(e, 1), pos(d, 2), pos(c, 3), pos(b, 4), pos(a, 5)]).
diagonal([pos(f, 1), pos(e, 2), pos(d, 3), pos(c, 4), pos(b, 5), pos(a, 6)]).
diagonal([pos(g, 1), pos(f, 2), pos(e, 3), pos(d, 4), pos(c, 5), pos(b, 6), pos(a, 7)]).
diagonal([pos(h, 1), pos(g, 2), pos(f, 3), pos(e, 4), pos(d, 5), pos(c, 6), pos(b, 7), pos(a, 8)]).
diagonal([pos(h, 2), pos(g, 3), pos(f, 4), pos(e, 5), pos(d, 6), pos(c, 7), pos(b, 8)]).
diagonal([pos(h, 3), pos(g, 4), pos(f, 5), pos(e, 6), pos(d, 7), pos(c, 8)]).
diagonal([pos(h, 4), pos(g, 5), pos(f, 6), pos(e, 7), pos(d, 8)]).
diagonal([pos(h, 5), pos(g, 6), pos(f, 7), pos(e, 8)]).
diagonal([pos(h, 6), pos(g, 7), pos(f, 8)]).
diagonal([pos(h, 7), pos(g, 8)]).
diagonal([pos(h, 8)]).

l_move(pos(FromColumn, FromRow), pos(ToColumn, ToRow)) :-
  l_move_helper(FromColumn, FromRow, ToColumn, ToRow, FromColumnNumber, ToColumnNumber),
  ToColumnNumber is FromColumnNumber + 1,
  ToRow is FromRow + 2.
l_move(pos(FromColumn, FromRow), pos(ToColumn, ToRow)) :-
  l_move_helper(FromColumn, FromRow, ToColumn, ToRow, FromColumnNumber, ToColumnNumber),
  ToColumnNumber is FromColumnNumber + 1,
  ToRow is FromRow - 2.
l_move(pos(FromColumn, FromRow), pos(ToColumn, ToRow)) :-
  l_move_helper(FromColumn, FromRow, ToColumn, ToRow, FromColumnNumber, ToColumnNumber),
  ToColumnNumber is FromColumnNumber - 1,
  ToRow is FromRow + 2.
l_move(pos(FromColumn, FromRow), pos(ToColumn, ToRow)) :-
  l_move_helper(FromColumn, FromRow, ToColumn, ToRow, FromColumnNumber, ToColumnNumber),
  ToColumnNumber is FromColumnNumber - 1,
  ToRow is FromRow - 2.
l_move(pos(FromColumn, FromRow), pos(ToColumn, ToRow)) :-
  l_move_helper(FromColumn, FromRow, ToColumn, ToRow, FromColumnNumber, ToColumnNumber),
  ToColumnNumber is FromColumnNumber + 2,
  ToRow is FromRow + 1.
l_move(pos(FromColumn, FromRow), pos(ToColumn, ToRow)) :-
  l_move_helper(FromColumn, FromRow, ToColumn, ToRow, FromColumnNumber, ToColumnNumber),
  ToColumnNumber is FromColumnNumber + 2,
  ToRow is FromRow - 1.
l_move(pos(FromColumn, FromRow), pos(ToColumn, ToRow)) :-
  l_move_helper(FromColumn, FromRow, ToColumn, ToRow, FromColumnNumber, ToColumnNumber),
  ToColumnNumber is FromColumnNumber - 2,
  ToRow is FromRow + 1.
l_move(pos(FromColumn, FromRow), pos(ToColumn, ToRow)) :-
  l_move_helper(FromColumn, FromRow, ToColumn, ToRow, FromColumnNumber, ToColumnNumber),
  ToColumnNumber is FromColumnNumber - 2,
  ToRow is FromRow - 1.

l_move_helper(FromColumn, FromRow, ToColumn, ToRow, FromColumnNumber, ToColumnNumber) :- 
  column(FromColumnNumber, FromColumn, _),
  column(ToColumnNumber, ToColumn, _),
  row(FromRow, _),
  row(ToRow, _).


% Helper functions:
adjacent(X, Y, [X,Y|_]).
adjacent(X, Y, [_|Tail]) :-
    adjacent(X, Y, Tail).


laneway_empty(Color, From, To, List, Board) :-
  laneway_empty_with_take(Color, From, To, List, Board).
laneway_empty(_, From, To, List, Board) :-
  laneway_empty_without_take(From, To, List, Board).

laneway_empty_with_take(Color, From, To, List, Board) :-
  laneway_empty_with_take_helper(Color, From, To, List, Board).
laneway_empty_with_take(Color, From, To, List, Board) :-
  reverse(List, ReversedList),
  laneway_empty_with_take_helper(Color, From, To, ReversedList, Board).

laneway_empty_with_take_helper(Color, From, To, List, Board) :-
  adjacent(From, To, List),
  member(piece(TakenColor, _, To), Board),
  dif(TakenColor, Color).

laneway_empty_with_take_helper(Color, From, To, List, Board) :-
  adjacent(From, NextPos, List),
  dif(To, NextPos),
  \+ member(piece(_, _, NextPos), Board),
  laneway_empty_with_take_helper(Color, NextPos, To, List, Board).


laneway_empty_without_take(From, To, List, Board) :-
  member(From, List), % Not sure if needed, ended in infinite loop when testing function on its own but might be able to remove
  member(To, List), % Not sure if needed, ended in infinite loop when testing function on its own but might be able to remove
  laneway_empty_without_take_helper(From, To, List, Board).
laneway_empty_without_take(From, To, List, Board) :-
  member(From, List), % Not sure if needed, ended in infinite loop when testing function on its own but might be able to remove
  member(To, List), % Not sure if needed, ended in infinite loop when testing function on its own but might be able to remove
  reverse(List, ReversedList),
  laneway_empty_without_take_helper(From, To, ReversedList, Board).

laneway_empty_without_take_helper(From, To, List, Board) :-
  adjacent(From, To, List),
  \+ member(piece(_, _, To), Board).

laneway_empty_without_take_helper(From, To, List, Board) :-
  adjacent(From, NextPos, List),
  dif(To, NextPos),
  \+ member(piece(_, _, NextPos), Board),
  laneway_empty_without_take_helper(NextPos, To, List, Board).
