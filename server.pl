%% Import HTTP Library
:- use_module(library(http/thread_httpd)).
:- use_module(library(http/http_dispatch)).
:- use_module(library(http/http_path)).
:- use_module(library(http/http_files)).
:- use_module(library(http/http_error)).
:- use_module(library(http/html_write)).
:- use_module(library(http/http_client)).
:- use_module(library(http/http_json)).
:- use_module(library(http/json)).
:- use_module(library(http/json_convert)).

%% Import Chess Engine
:- [chess].

%% Server declaration
server(Port) :-
  http_server(http_dispatch, [ port(Port) ]).

%% Serve static files
:- http_handler(root(.), http_reply_from_files('.', []), [prefix]).
:- http_handler('/', http_reply_file('index.html', []), []).
:- http_handler('/favicon.ico', faviconResponse, []).
faviconResponse(Request) :-
  http_reply_file('favicon.ico', [], Request).


%% Declare a handler, binding an HTTP path to a predicate.
:- http_handler('/api', api, []).
api(Request) :-
  member(method(post), Request), !,
  http_read_json(Request, JSON),
  parseMove(JSON, From, JSON2),
  parseMove(JSON2, To, JSON3),
  parseBoard(JSON3, Board, _),
  format('Content-type: text/html~n~n', []),
  move(From, To, Board, NewBoard),
  portray_clause(NewBoard).

%% Parse a To or From move
parseMove([ PieceData | L], Piece, L) :-
  parseMoveInner(PieceData, Piece).
parseMoveInner([Color, Type, [Col, Row]], piece(Color, Type, pos(Col, Row))).

%% Parse Board moves array
parseBoard([[ TurnColor, B ] | L], board(TurnColor, Board), L) :-
  parseBoardInner(B, Board).
parseBoardInner([], []).
parseBoardInner([ PieceData | L ], [ Piece | B ]) :-
  parseMoveInner(PieceData, Piece),
  parseBoardInner(L, B).

:- server(8312).